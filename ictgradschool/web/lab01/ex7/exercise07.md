* Cat
* Dog
* Horse
* Cow
* Polar Bear
* Sheep
* Frog
* Rat
* Hawk
* Lion
* Fox
* Capybara
* Raccoon
* Chicken
* Fennec Fox


### Herbivorous Animals

* Horse
* Rat
* Chicken
* Sheep
* Frog
* Cow
* Capybara

### Carnivorous Animals

* Cat
* Dog
* Polar Bear
* Lion

### Omnivorous Animals 

* Hawk
* Racoon
* Fennec Fox
* Fox
